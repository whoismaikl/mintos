package lv.mintos.transaction_service.controller.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import lv.mintos.shared_libraries.dto.TransferRequestDto;
import lv.mintos.transaction_service.service.TransactionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class TransactionControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TransactionService transactionService;

    private TransferRequestDto transferRequestDto;

    @BeforeEach
    public void setUp() {
        transferRequestDto = new TransferRequestDto();
        // Set the necessary values for your TransferRequestDto
    }

    @Test
    void transfer() throws Exception {
        String TRANSACTION_ID = "1234567890";
        Mockito.when(transactionService.initiateMoneyTransfer(Mockito.any())).thenReturn(TRANSACTION_ID);

        mockMvc.perform(MockMvcRequestBuilders.post("/v1/transactions/transfer")
                        .content(new ObjectMapper().writeValueAsString(transferRequestDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Transfer initiated with Id: " + TRANSACTION_ID));
    }

}