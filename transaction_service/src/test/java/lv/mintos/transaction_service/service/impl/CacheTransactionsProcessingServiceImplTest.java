package lv.mintos.transaction_service.service.impl;

import lv.mintos.shared_libraries.dto.TransferRequestDto;
import lv.mintos.transaction_service.caching.CacheService;
import lv.mintos.transaction_service.messaging.Publisher;
import lv.mintos.transaction_service.model.Transaction;
import lv.mintos.transaction_service.repository.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CacheTransactionsProcessingServiceImplTest {

    @Mock
    private Publisher publisher;

    @Mock
    private CacheService cacheService;

    @Mock
    private TransactionRepository transactionRepository;

    @InjectMocks
    private CacheTransactionsProcessingServiceImpl cacheTransactionsProcessingService;

    private Map<String, String> transactions;
    private final String transactionStatus = "TestStatus";
    private List<Transaction> transactionList;

    @BeforeEach
    void setUp() {
        transactions = new HashMap<>();
        transactions.put("550e8400-e29b-41d4-a716-446655440000", "value");

        Transaction transaction = new Transaction();
        transaction.setId(UUID.fromString("550e8400-e29b-41d4-a716-446655440000"));
        transaction.setSourceAccountId(UUID.randomUUID());
        transaction.setDestinationAccountId(UUID.randomUUID());
        transactionList = List.of(transaction);
    }

    @Test
    void updateTransactionsInDatabases() {

        when(cacheService.getTransactionResult(anyString())).thenReturn("TestResult");
        when(transactionRepository.findAllById(anyList())).thenReturn(transactionList);

        cacheTransactionsProcessingService.updateTransactionsInDatabases(transactions, transactionStatus);

        verify(cacheService, times(1)).evictTransactions(anyList());
        verify(cacheService, times(1)).evictTransactionsFromHash(anyList());
        verify(transactionRepository, times(1)).saveAll(anyList());
    }

    @Test
    void processActiveTransactions() {

        when(transactionRepository.findAllById(anyList())).thenReturn(transactionList);

        cacheTransactionsProcessingService.processActiveTransactions(transactions, transactionStatus);

        verify(publisher, times(transactionList.size())).sendTransferRequest(any(TransferRequestDto.class));
    }
}