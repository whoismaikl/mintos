package lv.mintos.transaction_service.caching;

import com.fasterxml.jackson.core.JsonProcessingException;
import lv.mintos.shared_libraries.caching.TransactionInfo;

import java.util.Collection;
import java.util.Map;

/**
 * Defines key operations for caching transaction information.
 */
public interface CacheService {

    /**
     * Stores a transaction Id along with its corresponding {@link TransactionInfo} in the cache.
     *
     * @param transactionId   the unique identifier of the transaction
     * @param transactionInfo the transaction details encapsulated in {@link TransactionInfo}
     */
    void saveTransactionStatus(String transactionId, TransactionInfo transactionInfo) throws JsonProcessingException;

    /**
     * Fetches the status of a specific transaction from the cache using its Id.
     *
     * @param transactionId the unique identifier of the transaction
     * @return the status of the transaction as a string,
     * or null if there's no matching transaction Id in the cache
     */
    String getTransactionStatus(String transactionId);

    /**
     * Stores the result of a completed transaction in the cache.
     *
     * @param transactionId the unique identifier of the transaction
     * @param result        the result of the transaction represented as a string
     */
    void saveTransactionResult(String transactionId, String result);

    /**
     * Retrieves the result of a completed transaction from the cache.
     *
     * @param transactionId the unique identifier of the transaction
     * @return the result of the transaction as a string,
     * or null if there's no matching transaction Id in the cache
     */
    String getTransactionResult(String transactionId);

    /**
     * Retrieves all transactions from the cache having a specific status.
     *
     * @param status the status identifier to filter transactions by
     * @return a map of transaction Ids to their respective statuses,
     * including only the transactions whose status matches the provided status
     */
    Map<String, String> getAllTransactionsByStatus(String status);

    /**
     * Removes transactions from the cache.
     *
     * @param ids the collection of transaction Ids to be removed from the cache
     */
    void evictTransactions(Collection<String> ids);

    /**
     * Removes transactions from hash structure in cache.
     *
     * @param ids the collection of transaction Ids to be removed from the hash in the cache
     */
    void evictTransactionsFromHash(Collection<String> ids);

    /**
     * Removes a specific transaction from the cache.
     *
     * @param id the unique identifier of the transaction to be removed from the cache
     */
    public void evictTransaction(String id);

    /**
     * Removes a specific transaction from the hash in the cache.
     *
     * @param id the unique identifier of the transaction to be removed from the hash in the cache
     */
    public void evictTransactionFromHash(String id);

    void deleteAllKeysForMap();
}