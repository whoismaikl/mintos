package lv.mintos.transaction_service.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import lv.mintos.shared_libraries.dto.TransactionsHistoryRequestDto;
import lv.mintos.shared_libraries.dto.TransactionsHistoryResponseDto;
import lv.mintos.shared_libraries.dto.TransferRequestDto;
import lv.mintos.transaction_service.model.Transaction;

import java.util.List;
import java.util.UUID;

public interface TransactionService {

    public String initiateMoneyTransfer(TransferRequestDto request) throws JsonProcessingException;

    public Transaction updateTransaction(UUID id, Transaction transaction);

    public Transaction saveTransactionEntity(Transaction transaction);

    List<TransactionsHistoryResponseDto> getAccountTransactions(TransactionsHistoryRequestDto transactionsHistoryRequestDto);

    Transaction getTransactionById(UUID id);

    void saveTransactionList(List<Transaction> transactions);

}
