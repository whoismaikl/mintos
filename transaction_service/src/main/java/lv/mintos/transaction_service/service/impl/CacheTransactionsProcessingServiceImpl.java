package lv.mintos.transaction_service.service.impl;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;
import lv.mintos.shared_libraries.dto.TransferRequestDto;
import lv.mintos.transaction_service.caching.CacheService;
import lv.mintos.transaction_service.mapper.TransactionsMapper;
import lv.mintos.transaction_service.messaging.Publisher;
import lv.mintos.transaction_service.model.Transaction;
import lv.mintos.transaction_service.repository.TransactionRepository;
import lv.mintos.transaction_service.service.CacheTransactionsProcessingService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CacheTransactionsProcessingServiceImpl implements CacheTransactionsProcessingService {

    private final Publisher publisher;

    private final CacheService cacheService;

    private final TransactionRepository transactionRepository;

    public CacheTransactionsProcessingServiceImpl(Publisher publisher, CacheService cacheService, TransactionRepository transactionRepository) {
        this.publisher = publisher;
        this.cacheService = cacheService;
        this.transactionRepository = transactionRepository;
    }

    @Transactional
    public void updateTransactionsInDatabases(Map<String, String> transactions, String transactionStatus) {
        //Getting UUIDs from cache transactions
        List<UUID> transactionUUIDs = transactions
                .keySet()
                .stream()
                .map(UUID::fromString)
                .toList();
        log.info("Transaction UUIDs from cache: {}", transactionUUIDs + " with status: " + transactionStatus);

        List<String> transactionUUIDStrings = new ArrayList<>(transactions.keySet());

        //Getting results map from cache
        Map<String, String> transactionResultsMap = transactionUUIDStrings
                .stream()
                .collect(Collectors.toMap(id -> id, id -> {
                    String result = cacheService.getTransactionResult(id);
                    return result != null ? result : "No Result";
                }));

        updatePostgresDb(transactionUUIDs, transactionResultsMap, transactionStatus);

        cacheService.evictTransactions(new ArrayList<>(transactions.keySet()));
        cacheService.evictTransactionsFromHash(new ArrayList<>(transactions.keySet()));
    }

    @Override
    public void processActiveTransactions(Map<String, String> pendingTransactions, String transactionStatus) {

        List<UUID> transactionUUIDs = pendingTransactions
                .keySet()
                .stream()
                .map(UUID::fromString)
                .toList();
        log.info("Transaction UUIDs from cache: {}", transactionUUIDs + " with status: " + transactionStatus);

        List<Transaction> dbTransactions = transactionRepository.findAllById(transactionUUIDs);

        List<TransferRequestDto> requestList = dbTransactions
                .stream()
                .map(TransactionsMapper::transactionToDto)
                .toList();

        requestList.forEach(request -> log.info("Processing " + transactionStatus + " requests: to client service " + request));

        requestList.forEach(publisher::sendTransferRequest);

    }

    private void updatePostgresDb(List<UUID> transactionUUIDs, Map<String, String> transactionResultsMap, String transactionStatus) {

        List<Transaction> dbTransactions = transactionRepository.findAllById(transactionUUIDs);
        log.info("Transaction count from Db: {}", dbTransactions.size());

        dbTransactions.forEach(transaction -> {
            transaction.setTransactionResult(transactionResultsMap.get(transaction.getId().toString()));
            transaction.setTransactionStatus(transactionStatus);
        });

        transactionRepository.saveAll(dbTransactions);
    }

}
