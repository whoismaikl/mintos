package lv.mintos.transaction_service.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import lv.mintos.shared_libraries.caching.TransactionInfo;
import lv.mintos.shared_libraries.dto.TransactionsHistoryRequestDto;
import lv.mintos.shared_libraries.dto.TransactionsHistoryResponseDto;
import lv.mintos.shared_libraries.dto.TransferRequestDto;
import lv.mintos.shared_libraries.enums.TransactionStatus;
import lv.mintos.transaction_service.caching.CacheService;
import lv.mintos.transaction_service.exception.TransactionNotFoundException;
import lv.mintos.transaction_service.mapper.TransactionsMapper;
import lv.mintos.transaction_service.messaging.Publisher;
import lv.mintos.transaction_service.model.Transaction;
import lv.mintos.transaction_service.repository.TransactionRepository;
import lv.mintos.transaction_service.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TransactionServiceImpl implements TransactionService {
    private final TransactionRepository transactionRepository;
    private final Publisher publisher;
    private final CacheService cacheService;

    @Autowired
    public TransactionServiceImpl(TransactionRepository transactionRepository, Publisher publisher, CacheService cacheService) {
        this.transactionRepository = transactionRepository;
        this.publisher = publisher;
        this.cacheService = cacheService;
    }

    @Override
    public String initiateMoneyTransfer(TransferRequestDto request) throws JsonProcessingException {

        String transactionId = UUID.randomUUID().toString();
        request.setTransactionId(transactionId);

        TransactionInfo transactionInfo = new TransactionInfo(TransactionStatus.INITIATED.getStatus(), "1");
        cacheService.saveTransactionStatus(request.getTransactionId(), transactionInfo);
        publisher.sendTransferRequest(request);

        Transaction transaction = TransactionsMapper.transactionToEntity(request);
        saveTransactionEntity(transaction);

        return transactionId;

    }

    @Override
    public Transaction updateTransaction(UUID id, Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    @Override
    public Transaction saveTransactionEntity(Transaction transaction) {
        return transactionRepository.save(transaction);
    }

    @Override
    public List<TransactionsHistoryResponseDto> getAccountTransactions(TransactionsHistoryRequestDto transactionsHistoryRequestDto) {

        UUID accountId = UUID.fromString(transactionsHistoryRequestDto.getAccountId());
        Pageable pageable = PageRequest.of(
                transactionsHistoryRequestDto.getOffset(), transactionsHistoryRequestDto.getLimit(),
                Sort.by(Sort.Direction.DESC, "timestamp")
        );
        List<Transaction> transactions = transactionRepository.findBySourceAccountId(accountId, pageable).getContent();
        return transactions.stream()
                .map(TransactionsMapper::historyToDto)
                .collect(Collectors.toList());
    }

    @Override
    public Transaction getTransactionById(UUID id) {
        return transactionRepository.findById(id).orElseThrow(() ->
                new TransactionNotFoundException("Transaction not found with id " + id));
    }

    @Override
    public void saveTransactionList(List<Transaction> transactions) {
        transactionRepository.saveAll(transactions);
    }

}
