package lv.mintos.transaction_service.service;

import java.util.Map;

public interface CacheTransactionsProcessingService {
    void updateTransactionsInDatabases(Map<String, String> transactions, String transactionStatus);

    void processActiveTransactions(Map<String, String> transactions, String transactionStatus);

}
