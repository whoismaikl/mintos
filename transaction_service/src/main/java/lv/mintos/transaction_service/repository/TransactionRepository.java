package lv.mintos.transaction_service.repository;

import lv.mintos.transaction_service.model.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, UUID> {
    Page<Transaction> findBySourceAccountId(UUID sourceAccountId, Pageable pageable);

    List<Transaction> findByTransactionStatus(String transactionStatus);

}
