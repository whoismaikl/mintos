package lv.mintos.transaction_service.controller.v1;

import com.fasterxml.jackson.core.JsonProcessingException;
import lv.mintos.shared_libraries.dto.TransferRequestDto;
import lv.mintos.transaction_service.service.TransactionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/transactions")
public class TransactionController {

    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping("/transfer")
    public ResponseEntity<String> transfer(@RequestBody TransferRequestDto request) throws JsonProcessingException {
        String transactionId = transactionService.initiateMoneyTransfer(request);
        return ResponseEntity.ok("Transfer initiated with Id: " + transactionId);
    }

}
