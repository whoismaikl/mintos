package lv.mintos.transaction_service.messaging.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import lv.mintos.shared_libraries.dto.TransactionsHistoryRequestDto;
import lv.mintos.shared_libraries.dto.TransactionsHistoryResponseDto;
import lv.mintos.transaction_service.messaging.Listener;
import lv.mintos.transaction_service.service.TransactionService;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
@Slf4j
public class ListenerImpl implements Listener {

    private final TransactionService transactionService;

    public ListenerImpl(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @RabbitListener(queues = "transactionQueue")
    @Override
    public Object receiveTransactionsHistoryRequest(String jsonRequest, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());
            TransactionsHistoryRequestDto transactionsHistoryRequestDto = mapper.readValue(jsonRequest, TransactionsHistoryRequestDto.class);

            log.info("Received TransactionsRequestDto: {}", transactionsHistoryRequestDto);
            List<TransactionsHistoryResponseDto> transactions = transactionService.getAccountTransactions(transactionsHistoryRequestDto);
            log.info("Returned from transaction repo with size " + transactions.size());

            return mapper.writeValueAsString(transactions);
        } catch (Exception e) {
            this.safeBasicReject(channel, tag);
            log.error("Unexpected error occurred in transaction listener", e);
            throw new AmqpRejectAndDontRequeueException(e);
        }
    }

    private void safeBasicReject(Channel channel, long tag) {
        try {
            channel.basicReject(tag, false);
        } catch (IOException e) {
            log.error("Failed to reject message", e);
        }
    }

}
