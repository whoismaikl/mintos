package lv.mintos.transaction_service.messaging;


import lv.mintos.shared_libraries.dto.TransferRequestDto;

public interface Publisher {
    void sendTransferRequest(TransferRequestDto transferRequestDto);

}
