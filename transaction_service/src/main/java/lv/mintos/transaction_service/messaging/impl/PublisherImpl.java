package lv.mintos.transaction_service.messaging.impl;

import lombok.extern.slf4j.Slf4j;
import lv.mintos.shared_libraries.dto.TransferRequestDto;
import lv.mintos.transaction_service.messaging.Publisher;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PublisherImpl implements Publisher {

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public PublisherImpl(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }
    @Override
    public void sendTransferRequest(TransferRequestDto transferRequestDto) {
        log.info("Sending transfer request.  Request: {}", transferRequestDto);
        rabbitTemplate.convertAndSend("clientQueue", transferRequestDto);
    }

}
