package lv.mintos.transaction_service.messaging;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;

public interface Listener {
    Object receiveTransactionsHistoryRequest(String jsonRequest, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag);
}
