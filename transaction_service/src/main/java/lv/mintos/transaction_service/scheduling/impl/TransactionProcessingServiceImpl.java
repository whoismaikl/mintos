package lv.mintos.transaction_service.scheduling.impl;

import lombok.extern.slf4j.Slf4j;
import lv.mintos.shared_libraries.enums.TransactionStatus;
import lv.mintos.transaction_service.caching.CacheService;
import lv.mintos.transaction_service.scheduling.TransactionProcessingService;
import lv.mintos.transaction_service.service.CacheTransactionsProcessingService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@Slf4j
public class TransactionProcessingServiceImpl implements TransactionProcessingService {
    public static final String KEY_STRING = "Key: ";
    public static final String VALUE_STRING = ", Value: ";
    private final CacheService cacheService;
    private final CacheTransactionsProcessingService cacheTransactionsProcessingService;

    public TransactionProcessingServiceImpl(CacheService cacheService, CacheTransactionsProcessingService cacheTransactionsProcessingService) {
        this.cacheService = cacheService;
        this.cacheTransactionsProcessingService = cacheTransactionsProcessingService;
    }

    @Scheduled(cron = "0 * * * * ?")
    public void handleTransactionsWithStatus() {
        //log.info("########## DEBUG ACTION: DELETING CACHE!");
        //cacheService.deleteAllKeysForMap();

        Map<String, String> completedTransactions = cacheService.getAllTransactionsByStatus(TransactionStatus.COMPLETED.getStatus());
        log.info("##################   COMPLETED:   ############################");
        completedTransactions.forEach((key, value) -> log.info(KEY_STRING + key + VALUE_STRING + value));
        cacheTransactionsProcessingService.updateTransactionsInDatabases(completedTransactions, TransactionStatus.COMPLETED.getStatus());

        Map<String, String> failedTransactions = cacheService.getAllTransactionsByStatus(TransactionStatus.FAILED.getStatus());
        log.info("##################    FAILED:     ############################");
        failedTransactions.forEach((key, value) -> log.info(KEY_STRING + key + VALUE_STRING + value));
        cacheTransactionsProcessingService.updateTransactionsInDatabases(failedTransactions, TransactionStatus.FAILED.getStatus());

        Map<String, String> pendingTransactions = cacheService.getAllTransactionsByStatus(TransactionStatus.RATE_PENDING.getStatus());
        log.info("##################  RATE_PENDING #############################");
        pendingTransactions.forEach((key, value) -> log.info(KEY_STRING + key + VALUE_STRING + value));
        cacheTransactionsProcessingService.processActiveTransactions(pendingTransactions, TransactionStatus.RATE_PENDING.getStatus());

        Map<String, String> transactionWithRate = cacheService.getAllTransactionsByStatus(TransactionStatus.RATE_ACQUIRED.getStatus());
        log.info("##################  RATE_ACQUIRED ############################");
        transactionWithRate.forEach((key, value) -> log.info(KEY_STRING + key + VALUE_STRING + value));
        cacheTransactionsProcessingService.processActiveTransactions(transactionWithRate, TransactionStatus.RATE_ACQUIRED.getStatus());

    }

}
