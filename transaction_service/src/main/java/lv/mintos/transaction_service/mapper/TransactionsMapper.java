package lv.mintos.transaction_service.mapper;


import lv.mintos.shared_libraries.dto.TransactionsHistoryResponseDto;
import lv.mintos.shared_libraries.dto.TransferRequestDto;
import lv.mintos.shared_libraries.enums.TransactionStatus;
import lv.mintos.transaction_service.model.Transaction;

import java.time.LocalDateTime;
import java.util.UUID;

public class TransactionsMapper {

    private TransactionsMapper(){
        throw new IllegalStateException("Mapper class");
    }
    public static Transaction transactionToEntity(TransferRequestDto dto) {

        return Transaction.builder()
                .id(UUID.fromString(dto.getTransactionId()))
                .sourceAccountId(UUID.fromString(dto.getSourceAccountId()))
                .destinationAccountId(UUID.fromString(dto.getDestinationAccountId()))
                .amount(dto.getAmount())
                .currencyCode(dto.getCurrencyCode())
                .timestamp(LocalDateTime.now())
                .description(dto.getDescription())
                .transactionStatus(TransactionStatus.INITIATED.getStatus())
                .transactionResult("")
                .build();
    }

    public static TransactionsHistoryResponseDto historyToDto(Transaction transaction) {
        return TransactionsHistoryResponseDto.builder()
                .id(transaction.getId().toString())
                .sourceAccountId(transaction.getSourceAccountId().toString())
                .destinationAccountId(transaction.getDestinationAccountId().toString())
                .currencyCode(transaction.getCurrencyCode())
                .amount(transaction.getAmount())
                .timestamp(transaction.getTimestamp())
                .description(transaction.getDescription())
                .transactionStatus(transaction.getTransactionStatus())
                .transactionResult(transaction.getTransactionResult())
                .build();
    }


    public static TransferRequestDto transactionToDto(Transaction transaction) {
        return new TransferRequestDto(
                transaction.getId().toString(),
                transaction.getSourceAccountId().toString(),
                transaction.getDestinationAccountId().toString(),
                transaction.getAmount(),
                transaction.getCurrencyCode(),
                transaction.getTimestamp(),
                transaction.getDescription()
        );
    }

}
