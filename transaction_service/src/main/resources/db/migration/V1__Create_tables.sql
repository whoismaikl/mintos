CREATE TABLE IF NOT EXISTS transactions
(
    id UUID PRIMARY KEY,
    source_account_id UUID NOT NULL,
    destination_account_id UUID NOT NULL,
    currency_code VARCHAR(10) NOT NULL,
    amount NUMERIC NOT NULL,
    timestamp TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    description TEXT,
    transaction_status VARCHAR(50),
    transaction_result VARCHAR(50)
);