package lv.mintos.client_service.controller.v1.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.activation.DataSource;
import lv.mintos.client_service.caching.CacheService;
import lv.mintos.client_service.controller.v1.ClientController;
import lv.mintos.client_service.dto.AccountDto;
import lv.mintos.client_service.enums.Currency;
import lv.mintos.client_service.service.AccountService;
import lv.mintos.shared_libraries.dto.TransactionsHistoryResponseDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ClientController.class)
@ExtendWith(SpringExtension.class)
class ClientControllerTest {

    @Autowired
    private MockMvc mvc;
    @MockBean
    private AccountService accountService;
    @MockBean
    CacheService cacheService;
    @MockBean
    protected DataSource dataSource;
    @MockBean
    protected RabbitTemplate rabbitTemplate;
    @MockBean
    protected RedisTemplate redisTemplate;

    @Test
    void getAllClients() throws Exception {
        UUID clientId = UUID.randomUUID();

        List<AccountDto> mockAccounts = Arrays.asList(
                new AccountDto(UUID.randomUUID(), clientId, BigDecimal.valueOf(1000), Currency.AED),
                new AccountDto(UUID.randomUUID(), clientId, BigDecimal.valueOf(2000), Currency.USD)
        );

        when(accountService.getClientAccounts(clientId)).thenReturn(mockAccounts);

        mvc.perform(get("/v1/clients/{clientId}/accounts", clientId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    void getClientAccountsNoAccountsExistReturnsEmptyList() throws Exception {
        UUID clientId = UUID.randomUUID();

        when(accountService.getClientAccounts(clientId))
                .thenReturn(new ArrayList<>());

        mvc.perform(get("/v1/clients/{clientId}/accounts", clientId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", is(0)));
    }

    @Test
    void getAccountTransactions() throws Exception {
        String accountId = "testAccountId";
        int offset = 0;
        int limit = 10;

        TransactionsHistoryResponseDto transactionsHistoryResponseDto = new TransactionsHistoryResponseDto();
        List<TransactionsHistoryResponseDto> transactions = Arrays.asList(transactionsHistoryResponseDto);

        ObjectMapper objectMapper = new ObjectMapper();
        String expectedJson = objectMapper.writeValueAsString(transactions);

        when(accountService.getAccountTransactions(accountId, offset, limit)).thenReturn(transactions);

        mvc.perform(get("/v1/clients/accounts/" + accountId + "/transactions"))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedJson));

        verify(accountService, times(1)).getAccountTransactions(accountId, offset, limit);
    }

}