package lv.mintos.client_service.service.impl;

import lv.mintos.client_service.dto.AccountDto;
import lv.mintos.client_service.model.Account;
import lv.mintos.client_service.model.Client;
import lv.mintos.client_service.enums.Currency;
import lv.mintos.client_service.repository.AccountRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    @Mock
    private AccountRepository accountRepository;

    @InjectMocks
    private AccountServiceImpl accountServiceImpl;

    @Test
    void getClientAccounts() {

        UUID clientId = UUID.randomUUID();
        Client sampleClient = new Client(clientId, "John Doe", "john.doe@example.com", null);
        List<Account> sampleAccountsFromRepository = Arrays.asList(
                new Account(UUID.randomUUID(), sampleClient, BigDecimal.valueOf(1000), Currency.AED),
                new Account(UUID.randomUUID(), sampleClient, BigDecimal.valueOf(2000), Currency.EUR)
        );

        when(accountRepository.findByClientId(clientId)).thenReturn(sampleAccountsFromRepository);

        List<AccountDto> expectedAccountDtos = sampleAccountsFromRepository.stream()
                .map(account -> new AccountDto(
                        account.getId(),
                        account.getClient().getId(),
                        account.getBalance(),
                        account.getCurrency()))
                .collect(Collectors.toList());

        List<AccountDto> result = accountServiceImpl.getClientAccounts(clientId);

        assertThat(result).isEqualTo(expectedAccountDtos);
    }

    @Test
    void getClientAccountsWhenNoAccountsFound() {
        UUID clientId = UUID.randomUUID();

        when(accountRepository.findByClientId(clientId)).thenReturn(Collections.emptyList());

        List<AccountDto> result = accountServiceImpl.getClientAccounts(clientId);

        assertThat(result).isEqualTo(Collections.emptyList());
    }

}