-- Clients
INSERT INTO clients (id, name, email)
VALUES
    ('a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 'John Doe', 'john.doe@example.com'),
    ('b0eebc99-9c0b-4ef8-bb6d-6bb9bd380a21', 'Jane Doe', 'jane.doe@example.com'),
    ('c0eebc99-9c0b-4ef8-bb6d-6bb9bd380a31', 'Alice Smith', 'alice.smith@example.com'),
    ('d0eebc99-9c0b-4ef8-bb6d-6bb9bd380a41', 'Bob Johnson', 'bob.johnson@example.com'),
    ('e0eebc99-9c0b-4ef8-bb6d-6bb9bd380a51', 'Charlie Brown', 'charlie.brown@example.com');

-- Accounts
INSERT INTO accounts (id, client_id, balance, currency)
VALUES
    -- Accounts for John Doe
    ('a1eebc99-9c0b-4ef8-bb6d-6bb9bd380a52', 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 10000.00, 'USD'),
    ('a1eebc99-9c0b-4ef8-bb6d-6bb9bd380a53', 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 15000.00, 'EUR'),
    ('a1eebc99-9c0b-4ef8-bb6d-6bb9bd380a54', 'a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11', 20000.00, 'GBP'),

    -- Accounts for Jane Doe
    ('b1eebc99-9c0b-4ef8-bb6d-6bb9bd380a62', 'b0eebc99-9c0b-4ef8-bb6d-6bb9bd380a21', 11000.00, 'USD'),
    ('b1eebc99-9c0b-4ef8-bb6d-6bb9bd380a63', 'b0eebc99-9c0b-4ef8-bb6d-6bb9bd380a21', 16000.00, 'EUR'),
    ('b1eebc99-9c0b-4ef8-bb6d-6bb9bd380a64', 'b0eebc99-9c0b-4ef8-bb6d-6bb9bd380a21', 21000.00, 'GBP'),

    -- Accounts for Alice Smith
    ('c1eebc99-9c0b-4ef8-bb6d-6bb9bd380a72', 'c0eebc99-9c0b-4ef8-bb6d-6bb9bd380a31', 12000.00, 'USD'),
    ('c1eebc99-9c0b-4ef8-bb6d-6bb9bd380a73', 'c0eebc99-9c0b-4ef8-bb6d-6bb9bd380a31', 17000.00, 'EUR'),
    ('c1eebc99-9c0b-4ef8-bb6d-6bb9bd380a74', 'c0eebc99-9c0b-4ef8-bb6d-6bb9bd380a31', 22000.00, 'GBP'),

    -- Accounts for Bob Johnson
    ('d1eebc99-9c0b-4ef8-bb6d-6bb9bd380a82', 'd0eebc99-9c0b-4ef8-bb6d-6bb9bd380a41', 13000.00, 'USD'),
    ('d1eebc99-9c0b-4ef8-bb6d-6bb9bd380a83', 'd0eebc99-9c0b-4ef8-bb6d-6bb9bd380a41', 18000.00, 'EUR'),
    ('d1eebc99-9c0b-4ef8-bb6d-6bb9bd380a84', 'd0eebc99-9c0b-4ef8-bb6d-6bb9bd380a41', 23000.00, 'GBP'),

    -- Accounts for Charlie Brown
    ('e1eebc99-9c0b-4ef8-bb6d-6bb9bd380a92', 'e0eebc99-9c0b-4ef8-bb6d-6bb9bd380a51', 14000.00, 'USD'),
    ('e1eebc99-9c0b-4ef8-bb6d-6bb9bd380a93', 'e0eebc99-9c0b-4ef8-bb6d-6bb9bd380a51', 19000.00, 'EUR'),
    ('e1eebc99-9c0b-4ef8-bb6d-6bb9bd380a94', 'e0eebc99-9c0b-4ef8-bb6d-6bb9bd380a51', 24000.00, 'GBP');