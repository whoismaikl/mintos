CREATE TABLE clients (
                         id UUID NOT NULL PRIMARY KEY,
                         name VARCHAR(55) NOT NULL,
                         email VARCHAR(77) UNIQUE NOT NULL
);

CREATE TABLE accounts (
                          id UUID NOT NULL PRIMARY KEY,
                          client_id UUID NOT NULL,
                          balance DECIMAL(19, 2),
                          currency VARCHAR(3),
                          FOREIGN KEY (client_id) REFERENCES clients (id)
);