package lv.mintos.client_service.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import lv.mintos.client_service.dto.AccountDto;
import lv.mintos.shared_libraries.dto.TransactionsHistoryResponseDto;
import lv.mintos.shared_libraries.dto.TransferRequestDto;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

public interface AccountService {
    List<AccountDto> getClientAccounts(UUID clientId);

    void processFundTransferRequest(TransferRequestDto transferRequestDto) throws JsonProcessingException;

    List<TransactionsHistoryResponseDto> getAccountTransactions(String accountId, int offset, int limit) throws IOException;

}
