package lv.mintos.client_service.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import lv.mintos.client_service.caching.CacheService;
import lv.mintos.client_service.dto.AccountDto;
import lv.mintos.client_service.enums.Currency;
import lv.mintos.client_service.exception.AccountNotFoundException;
import lv.mintos.client_service.exception.NegativeAmountException;
import lv.mintos.client_service.exception.TransactionStatusException;
import lv.mintos.client_service.exception.UnsupportedCurrencyException;
import lv.mintos.client_service.messaging.Publisher;
import lv.mintos.client_service.model.Account;
import lv.mintos.client_service.repository.AccountRepository;
import lv.mintos.client_service.service.AccountService;
import lv.mintos.shared_libraries.caching.TransactionInfo;
import lv.mintos.shared_libraries.dto.ExchangeRateDto;
import lv.mintos.shared_libraries.dto.TransactionsHistoryRequestDto;
import lv.mintos.shared_libraries.dto.TransactionsHistoryResponseDto;
import lv.mintos.shared_libraries.dto.TransferRequestDto;
import lv.mintos.shared_libraries.enums.TransactionStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {
    private final Publisher publisher;

    private final AccountRepository accountRepository;
    private final CacheService cacheService;

    public AccountServiceImpl(AccountRepository accountRepository, CacheService cacheService, Publisher publisher) {
        this.accountRepository = accountRepository;
        this.cacheService = cacheService;
        this.publisher = publisher;
    }

    @Override
    public List<AccountDto> getClientAccounts(UUID clientId) {
        return accountRepository.findByClientId(clientId).stream()
                .map(account -> new AccountDto(
                        account.getId(),
                        account.getClient().getId(),
                        account.getBalance(),
                        account.getCurrency()))
                .toList();
    }

    @Override
    public void processFundTransferRequest(TransferRequestDto transferRequestDto) throws JsonProcessingException {
        log.info("processFundTransferRequest");
        String transactionId = transferRequestDto.getTransactionId();

        checkTransactionStatus(transactionId);
        checkIsCurrencySupported(transferRequestDto);
        checkIsTransferAmountPositive(transferRequestDto);

        Optional<Account> sourceAccount = accountRepository.findAccountById(UUID.fromString(transferRequestDto.getSourceAccountId()));
        Optional<Account> destinationAccount = accountRepository.findAccountById(UUID.fromString(transferRequestDto.getDestinationAccountId()));

        checkAccountsExistence(sourceAccount, destinationAccount);

        Account actualSourceAccount = sourceAccount.orElseThrow(() ->
                new AccountNotFoundException("Source account not found"));
        Account actualDestinationAccount = destinationAccount.orElseThrow(() ->
                new AccountNotFoundException("Destination account not found"));

        checkReceiversAccountCurrencyForTransfer(transferRequestDto.getCurrencyCode(), actualDestinationAccount.getCurrency().getCode());


        if (actualSourceAccount.getCurrency().equals(actualDestinationAccount.getCurrency())) {
            log.info("Processing transfer with same currencies");
            processTransfer(actualSourceAccount,
                    actualDestinationAccount,
                    transferRequestDto.getAmount(),
                    transferRequestDto.getTransactionId(),
                    new BigDecimal("1.0"));

        } else {
            TransactionInfo transactionInfo = cacheService.getTransactionInfo(transactionId);
            if (transactionInfo.getStatus().equals(TransactionStatus.RATE_ACQUIRED.getStatus())) {
                processTransfer(actualSourceAccount,
                        actualDestinationAccount,
                        transferRequestDto.getAmount(),
                        transferRequestDto.getTransactionId(),
                        new BigDecimal(transactionInfo.getRate()));
            } else {
                ExchangeRateDto rateDtoRequest = generateExchangeRateDto(actualSourceAccount, actualDestinationAccount, transactionId);
                publisher.sendExchangeRateRequest(rateDtoRequest);
            }
        }

    }

    private static ExchangeRateDto generateExchangeRateDto(Account actualSourceAccount, Account actualDestinationAccount, String transactionId) {
        ExchangeRateDto rateDtoRequest = new ExchangeRateDto();
        rateDtoRequest.setBaseCurrency(actualSourceAccount.getCurrency().toString());
        rateDtoRequest.setTargetCurrency(actualDestinationAccount.getCurrency().toString());
        rateDtoRequest.setRate(new BigDecimal("1.0"));
        rateDtoRequest.setTransactionId(transactionId);
        return rateDtoRequest;
    }

    private void checkTransactionStatus(String transactionId) throws JsonProcessingException {
        String transactionStatusString = cacheService.getTransactionStatus(transactionId);
        log.info("transactionStatusString " + transactionStatusString);

        if (!Objects.equals(transactionStatusString, TransactionStatus.INITIATED.getStatus())
                && !Objects.equals(transactionStatusString, TransactionStatus.RATE_PENDING.getStatus())
                && !Objects.equals(transactionStatusString, TransactionStatus.RATE_ACQUIRED.getStatus())) {
            throw new TransactionStatusException(transactionId, transactionStatusString);
        }
    }

    public List<TransactionsHistoryResponseDto> getAccountTransactions(String accountId, int offset, int limit) throws JsonProcessingException {
        TransactionsHistoryRequestDto request = new TransactionsHistoryRequestDto(accountId, offset, limit, UUID.randomUUID().toString());

        Object responseObject = publisher.sendTransactionsHistoryRequest(request);
        if (responseObject instanceof String jsonResponse) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.registerModule(new JavaTimeModule());

            return mapper.readValue(jsonResponse,
                    TypeFactory.defaultInstance().constructCollectionType(List.class, TransactionsHistoryResponseDto.class));
        } else {
            throw new IllegalStateException("Unexpected response type: " + responseObject.getClass());
        }

    }

    private void processTransfer(Account actualSourceAccount,
                                 Account actualDestinationAccount,
                                 BigDecimal transferAmount,
                                 String transactionId,
                                 BigDecimal rate) throws JsonProcessingException {

        if (actualSourceAccount.getBalance().compareTo(transferAmount) < 0) {
            throw new IllegalStateException("Insufficient funds in source account");
        }

        BigDecimal newSourceBalance = actualSourceAccount.getBalance().subtract(transferAmount.divide(rate, RoundingMode.HALF_EVEN));
        actualSourceAccount.setBalance(newSourceBalance);

        BigDecimal newDestinationBalance = actualDestinationAccount.getBalance().add(transferAmount.multiply(rate));
        actualDestinationAccount.setBalance(newDestinationBalance);

        saveNewBalances(actualSourceAccount, actualDestinationAccount);
        saveTransactionResult(transactionId, newSourceBalance);

    }

    private void saveTransactionResult(String transactionId, BigDecimal newSourceBalance) throws JsonProcessingException {
        TransactionInfo info = new TransactionInfo(TransactionStatus.COMPLETED.getStatus(), "1");

        cacheService.saveTransactionStatus(transactionId, info);
        cacheService.saveTransactionResult(transactionId, newSourceBalance.toString());
    }

    private void saveNewBalances(Account actualSourceAccount, Account actualDestinationAccount) {
        log.info("Saving accounts with: source acc id" + actualSourceAccount.getId() + " : dest acc id" + actualDestinationAccount.getId());
        accountRepository.save(actualSourceAccount);
        accountRepository.save(actualDestinationAccount);
    }

    private static void checkIsTransferAmountPositive(TransferRequestDto transferRequestDto) {
        if (transferRequestDto.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            throw new NegativeAmountException("Transfer amount must be greater than zero");
        }
    }

    private static void checkIsCurrencySupported(TransferRequestDto transferRequestDto) {
        String currency = transferRequestDto.getCurrencyCode();
        if (Arrays.stream(Currency.values()).noneMatch(c -> c.name().equals(currency))) {
            throw new UnsupportedCurrencyException("The currency " + currency + " is not supported");
        }
    }

    private static void checkAccountsExistence(Optional<Account> sourceAccount, Optional<Account> destinationAccount) {
        if (sourceAccount.isEmpty() || destinationAccount.isEmpty()) {
            String message = "Accounts not found: " +
                    (sourceAccount.isEmpty() ? "Source account " : "") +
                    (destinationAccount.isEmpty() ? "Destination account" : "");
            throw new AccountNotFoundException(message);
        }
    }

    private static void checkReceiversAccountCurrencyForTransfer(String transferCurrency, String destinationCurrency) {
        if (!transferCurrency.equals(destinationCurrency)) {
            throw new UnsupportedCurrencyException("Transfer currency must match receivers account currency!");
        }
    }

}
