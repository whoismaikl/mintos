package lv.mintos.client_service.exception;

public class TransactionStatusException extends RuntimeException {

    public TransactionStatusException(String transactionId, String status) {
        super("The transaction with id: " + transactionId + " has already been processed with status: " + status + ".");
    }
}
