package lv.mintos.client_service.controller.v1;

import lv.mintos.client_service.dto.AccountDto;
import lv.mintos.client_service.service.AccountService;
import lv.mintos.shared_libraries.dto.TransactionsHistoryResponseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/v1/clients")
public class ClientController {

    private final AccountService accountService;

    public ClientController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/{clientId}/accounts")
    public ResponseEntity<List<AccountDto>> getClientAccounts(@PathVariable UUID clientId) {
        return ResponseEntity.ok(accountService.getClientAccounts(clientId));
    }

    @GetMapping("/accounts/{accountId}/transactions")
    public ResponseEntity<List<TransactionsHistoryResponseDto>> getAccountTransactions(
            @PathVariable String accountId,
            @RequestParam(defaultValue = "0") int offset,
            @RequestParam(defaultValue = "10") int limit) throws IOException {

        List<TransactionsHistoryResponseDto> transactions = accountService.getAccountTransactions(accountId, offset, limit);
        return ResponseEntity.ok(transactions);
    }

}
