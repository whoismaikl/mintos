package lv.mintos.client_service.repository;

import lv.mintos.client_service.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface AccountRepository extends JpaRepository<Account, UUID> {
    List<Account> findByClientId(UUID clientId);

    Optional<Account> findAccountById(UUID accountId);

}
