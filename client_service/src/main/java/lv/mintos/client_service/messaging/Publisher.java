package lv.mintos.client_service.messaging;


import com.fasterxml.jackson.core.JsonProcessingException;
import lv.mintos.shared_libraries.dto.ExchangeRateDto;
import lv.mintos.shared_libraries.dto.TransactionsHistoryRequestDto;

public interface Publisher {

    Object sendTransactionsHistoryRequest(TransactionsHistoryRequestDto request) throws JsonProcessingException;

    void sendExchangeRateRequest(ExchangeRateDto request) throws JsonProcessingException;

}
