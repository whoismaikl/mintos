package lv.mintos.client_service.messaging;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.rabbitmq.client.Channel;
import lv.mintos.shared_libraries.dto.TransferRequestDto;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;

public interface Listener {

    void handleTransferRequest(TransferRequestDto message, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws JsonProcessingException;

}
