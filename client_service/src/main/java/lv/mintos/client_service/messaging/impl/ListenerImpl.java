package lv.mintos.client_service.messaging.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import lv.mintos.client_service.caching.CacheService;
import lv.mintos.client_service.exception.AccountNotFoundException;
import lv.mintos.client_service.exception.NegativeAmountException;
import lv.mintos.client_service.exception.TransactionStatusException;
import lv.mintos.client_service.exception.UnsupportedCurrencyException;
import lv.mintos.client_service.messaging.Listener;
import lv.mintos.client_service.service.AccountService;
import lv.mintos.shared_libraries.caching.TransactionInfo;
import lv.mintos.shared_libraries.dto.TransferRequestDto;
import lv.mintos.shared_libraries.enums.TransactionStatus;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class ListenerImpl implements Listener {

    private final AccountService accountService;

    private final CacheService cacheService;

    public ListenerImpl(AccountService accountService, CacheService cacheService) {
        this.accountService = accountService;
        this.cacheService = cacheService;
    }

    @Override
//    @RabbitListener(queues = "${rabbitmq.transfer-request-queue-name}", ackMode = "MANUAL")
    @RabbitListener(queues = "clientQueue")
    public void handleTransferRequest(TransferRequestDto transferRequestDto, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws JsonProcessingException {
        try {
            log.info("message received in transfer-request-queue " + transferRequestDto);
            accountService.processFundTransferRequest(transferRequestDto);
        } catch (AccountNotFoundException e) {
            cacheService.saveTransactionStatus(transferRequestDto.getTransactionId(),
                    new TransactionInfo(TransactionStatus.FAILED.getStatus(), "1"));
            cacheService.saveTransactionResult(transferRequestDto.getTransactionId(), TransactionStatus.FAILED.getStatus());
            log.error("Account not found", e);
            this.safeBasicReject(channel, tag);
        } catch (NegativeAmountException e) {
            cacheService.saveTransactionStatus(transferRequestDto.getTransactionId(),
                    new TransactionInfo(TransactionStatus.FAILED.getStatus(), "1"));
            cacheService.saveTransactionResult(transferRequestDto.getTransactionId(), TransactionStatus.FAILED.getStatus());
            log.error("Negative amount", e);
            this.safeBasicReject(channel, tag);
        } catch (TransactionStatusException e) {
            cacheService.saveTransactionStatus(transferRequestDto.getTransactionId(),
                    new TransactionInfo(TransactionStatus.FAILED.getStatus(), "1"));
            cacheService.saveTransactionResult(transferRequestDto.getTransactionId(), TransactionStatus.FAILED.getStatus());
            log.error("Invalid transaction status", e);
            this.safeBasicReject(channel, tag);
        } catch (UnsupportedCurrencyException e) {
            cacheService.saveTransactionStatus(transferRequestDto.getTransactionId(),
                    new TransactionInfo(TransactionStatus.FAILED.getStatus(), "1"));
            cacheService.saveTransactionResult(transferRequestDto.getTransactionId(), TransactionStatus.FAILED.getStatus());
            log.error("Unsupported currency", e);
            this.safeBasicReject(channel, tag);
        } catch (IllegalStateException e) {
            cacheService.saveTransactionStatus(transferRequestDto.getTransactionId(),
                    new TransactionInfo(TransactionStatus.FAILED.getStatus(), "1"));
            cacheService.saveTransactionResult(transferRequestDto.getTransactionId(), TransactionStatus.FAILED.getStatus());
            log.error("Illegal State", e);
            this.safeBasicReject(channel, tag);
        } catch (Exception e) {
            cacheService.saveTransactionStatus(transferRequestDto.getTransactionId(),
                    new TransactionInfo(TransactionStatus.FAILED.getStatus(), "1"));
            cacheService.saveTransactionResult(transferRequestDto.getTransactionId(), TransactionStatus.FAILED.getStatus());
            log.error("Unexpected error occurred in client service listener", e);
            throw new AmqpRejectAndDontRequeueException(e);
        }
    }

    private void safeBasicReject(Channel channel, long tag) {
        try {
            channel.basicReject(tag, false);
        } catch (IOException e) {
            log.error("Failed to reject message", e);
        }
    }
}
