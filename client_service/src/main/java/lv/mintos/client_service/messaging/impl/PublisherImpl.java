package lv.mintos.client_service.messaging.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lv.mintos.client_service.messaging.Publisher;
import lv.mintos.shared_libraries.dto.ExchangeRateDto;
import lv.mintos.shared_libraries.dto.TransactionsHistoryRequestDto;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PublisherImpl implements Publisher {
    private final RabbitTemplate rabbitTemplate;

    public PublisherImpl(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public Object sendTransactionsHistoryRequest(TransactionsHistoryRequestDto request) throws JsonProcessingException {
        log.info("Sending TransactionsRequestDto. Request: {}", request);
        String jsonRequest = new ObjectMapper().writeValueAsString(request);
        return rabbitTemplate.convertSendAndReceive("transactionQueue", jsonRequest);
    }

    @Override
    public void sendExchangeRateRequest(ExchangeRateDto rateRequest) {
        log.info("Request: " + rateRequest.getBaseCurrency());
        rabbitTemplate.convertAndSend("currencyQueue", rateRequest);

    }

}
