package lv.mintos.client_service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lv.mintos.client_service.enums.Currency;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDto {

    private UUID id;
    private UUID clientId;
    private BigDecimal balance;
    private Currency currency;

}
