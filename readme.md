# Home Task

The project contains three microservices with separate databases: Redis as a distributed cache and RabbitMQ as the message broker. All services are Dockerized. To start, run the root Docker Compose file - it will start all services.


The Client DB preloads some clients and accounts. Different services expose to different ports (you can see this in the root `docker-compose.yaml` file).

Below are the HTTP requests to interact with the system:

## Fetch Client Accounts

GET Request: `http://localhost:8080/v1/clients/a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11/accounts`

You can choose any ID from the preloaded list (there is no functionality for creating new clients or accounts as per the requirements).

## Make a Transfer

POST Request: `http://localhost:8081/v1/transactions/transfer`

Body (choose preferred accounts from the DB):
json { "sourceAccountId": "a1eebc99-9c0b-4ef8-bb6d-6bb9bd380a52", "destinationAccountId": "b1eebc99-9c0b-4ef8-bb6d-6bb9bd380a62", "amount": 32 , "currencyCode": "USD", "description": "test" }


## Fetch Transaction History

GET Request: `http://localhost:8080/v1/clients/accounts/a1eebc99-9c0b-4ef8-bb6d-6bb9bd380a52/transactions`

Before fetching the transaction history, you need to make some transactions (no preloaded transactions available).

## Database connections:

- Client DB: `jdbc:postgresql://localhost:5432/client_db`
- Transaction DB: `jdbc:postgresql://localhost:5433/transaction_db`

Connection credentials are in the root `docker-compose.yaml` file.

### Possible improvements:
1 - While sending money transfer check that Account balance doesn`t fall <0
(Balance - all pending or initiated transactions - transaction amount)
####
2 - Add more unit tests. Add integration tests. 
