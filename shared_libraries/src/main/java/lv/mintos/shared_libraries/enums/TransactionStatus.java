package lv.mintos.shared_libraries.enums;

import lombok.Getter;

@Getter
public enum TransactionStatus {
    INITIATED("INITIATED"),
    RATE_ACQUIRED("RATE_ACQUIRED"),
    COMPLETED("COMPLETED"),
    FAILED("FAILED"),
    RATE_PENDING("RATE_PENDING");

    private final String status;

    TransactionStatus(String status) {
        this.status = status;
    }
}
