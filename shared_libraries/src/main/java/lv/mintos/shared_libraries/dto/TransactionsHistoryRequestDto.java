package lv.mintos.shared_libraries.dto;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransactionsHistoryRequestDto implements Serializable {

    private String accountId;
    private int offset;
    private int limit;
    private String correlationId;

}
