package lv.mintos.shared_libraries.dto;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class TransferRequestDto implements Serializable {

    String transactionId;
    String sourceAccountId;
    String destinationAccountId;
    BigDecimal amount;
    String currencyCode;
    LocalDateTime timestamp;
    String description;

}
