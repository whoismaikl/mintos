package lv.mintos.shared_libraries.dto;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransactionsHistoryResponseDto implements Serializable {

    private String id;
    private String sourceAccountId;
    private String destinationAccountId;
    private String currencyCode;
    private BigDecimal amount;
    private LocalDateTime timestamp;
    private String description;
    private String transactionStatus;
    private String transactionResult;

}
