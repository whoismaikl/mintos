package lv.mintos.shared_libraries.dto;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ExchangeRateDto implements Serializable {

    private String transactionId;
    private String baseCurrency;
    private String targetCurrency;
    private BigDecimal rate;

}
