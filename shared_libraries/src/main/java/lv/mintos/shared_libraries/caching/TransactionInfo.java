package lv.mintos.shared_libraries.caching;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TransactionInfo implements Serializable {
    private String status;
    private String rate;
}
