package lv.mintos.currency_exchange_service.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lv.mintos.currency_exchange_service.caching.CacheService;
import lv.mintos.currency_exchange_service.service.CurrencyExchangeService;
import lv.mintos.shared_libraries.caching.TransactionInfo;
import lv.mintos.shared_libraries.dto.ExchangeRateDto;
import lv.mintos.shared_libraries.enums.TransactionStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
@Slf4j
public class CurrencyExchangeServiceImpl implements CurrencyExchangeService {

    @Value("${api.key}")
    private String apiKey;

    final CacheService cacheService;
    public static final String EXCHANGE_RATE_API_URL = "https://v6.exchangerate-api.com/v6/";

    public CurrencyExchangeServiceImpl(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @Retryable(backoff = @Backoff(delay = 300))
    public void getExchangeRate(ExchangeRateDto exchangeRateDto) {

        String requestUrl = EXCHANGE_RATE_API_URL + apiKey + "/pair/"
                + exchangeRateDto.getBaseCurrency() + "/" + exchangeRateDto.getTargetCurrency();
        log.info("Request URL " + requestUrl);
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.getForObject(requestUrl, String.class);

        processResponse(response, exchangeRateDto);

    }

    @Recover
    public void recover(ExchangeRateDto exchangeRateDto) {

        log.info("Recover logic occurred");
        try {
            cacheService.saveTransactionStatus(exchangeRateDto.getTransactionId(),
                    new TransactionInfo(TransactionStatus.RATE_PENDING.getStatus(), "1"));
        } catch (JsonProcessingException ex) {
            throw new RuntimeException(ex);
        }

    }

    private void processResponse(String response, ExchangeRateDto exchangeRateDto) {
        try {
            Double rate = getRateFromResponse(response);
            TransactionInfo info = new TransactionInfo(TransactionStatus.RATE_ACQUIRED.getStatus(), rate.toString());
            log.info("rate received: {} ", rate);
            cacheService.saveTransactionStatus(exchangeRateDto.getTransactionId(), info);
        } catch (JsonProcessingException ex) {
            try {
                TransactionInfo info = new TransactionInfo(TransactionStatus.FAILED.getStatus(), "0");
                cacheService.saveTransactionResult(exchangeRateDto.getTransactionId(), TransactionStatus.FAILED.getStatus());
                cacheService.saveTransactionStatus(exchangeRateDto.getTransactionId(), info);
            } catch (JsonProcessingException e) {
                log.info("Cache service threw an error for transaction with ID: " + exchangeRateDto.getTransactionId());
                throw new RuntimeException(e);
            }
        }

    }

    private static Double getRateFromResponse(String response) {

        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> responseMap;
        try {
            responseMap = mapper.readValue(response, Map.class);
        } catch (JsonProcessingException e) {
            log.info("Error parsing json from exchange response");
            throw new RuntimeException(e);
        }
        return (Double) responseMap.get("conversion_rate");

    }

}
