package lv.mintos.currency_exchange_service.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import lv.mintos.shared_libraries.dto.ExchangeRateDto;

public interface CurrencyExchangeService {
    void getExchangeRate(ExchangeRateDto exchangeRateDto) throws JsonProcessingException;

}
