package lv.mintos.currency_exchange_service.caching.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lv.mintos.currency_exchange_service.caching.CacheService;
import lv.mintos.shared_libraries.caching.TransactionInfo;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CacheServiceImpl implements CacheService {

    private static final String TRANSACTION_STATUS_MAP_KEY = "TransactionStatusMap";
    private final HashOperations<String, String, String> hashOps;
    private final StringRedisTemplate stringRedisTemplate;

    public CacheServiceImpl(StringRedisTemplate stringRedisTemplate) {
        this.stringRedisTemplate = stringRedisTemplate;
        this.hashOps = this.stringRedisTemplate.opsForHash();
        log.info("Cache Service has been initialized.");
    }

    @Override
    public void saveTransactionStatus(String transactionId, TransactionInfo transactionInfo) throws JsonProcessingException {
        log.info("Saving transaction info. Transaction ID: {}, Status: {}", transactionId, transactionInfo);
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = mapper.writeValueAsString(transactionInfo);
        hashOps.put(TRANSACTION_STATUS_MAP_KEY, transactionId, jsonString);
        log.info("Transaction status saved.");
    }

    @Override
    public String getTransactionStatus(String transactionId) {

        log.info("Retrieving transaction status for Transaction ID: {}", transactionId);
        String jsonValue = hashOps.get(TRANSACTION_STATUS_MAP_KEY, transactionId);
        log.info("Retrieved Transaction Status: {}", jsonValue);

        return jsonValue;
    }

    @Override
    public void saveTransactionResult(String transactionId, String result) {
        log.info("Saving transaction result for Transaction ID: {}, Result: {}", transactionId, result);
        stringRedisTemplate.opsForHash().put(transactionId, "result", result);
        log.info("Transaction result saved.");
    }

    @Override
    public String getTransactionResult(String transactionId) {
        log.info("Retrieving transaction result for Transaction ID: {}", transactionId);

        String result = (String) stringRedisTemplate.opsForHash().get(transactionId, "result");

        log.info("Retrieved Transaction Result: {}", result);
        return result;
    }

    @Override
    public Map<String, String> getAllTransactionsByStatus(String status) {
        log.info("Retrieving all transactions with Status: {}", status);
        ObjectMapper mapper = new ObjectMapper();

        Map<String, String> allTransactions = hashOps.entries(TRANSACTION_STATUS_MAP_KEY);
        Map<String, String> filteredTransactions = allTransactions.entrySet().stream()
                .map(entry -> {
                    try {
                        TransactionInfo info = mapper.readValue(entry.getValue(), TransactionInfo.class);

                        return new AbstractMap.SimpleEntry<>(entry.getKey(), info.getStatus());
                    } catch (IOException e) {
                        throw new RuntimeException(
                                String.format("Failed to deserialize TransactionInfo for transaction ID: %s", entry.getKey()),
                                e
                        );
                    }
                })
                .filter(entry -> status.equals(entry.getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        log.info("Retrieved {} transactions.", filteredTransactions.size());
        return filteredTransactions;
    }

    @Override
    public void evictTransactions(Collection<String> ids) {
        if (ids != null && !ids.isEmpty()) {
            log.info("Evicting transactions for IDs: {}", String.join(",", ids));

            stringRedisTemplate.delete(ids);

            log.info("Transactions evicted.");
        }
    }

    @Override
    public void evictTransactionsFromHash(Collection<String> ids) {
        if (ids != null && !ids.isEmpty()) {
            log.info("Evicting transactions from hash for IDs: {}", String.join(",", ids));

            hashOps.delete(TRANSACTION_STATUS_MAP_KEY, ids.toArray());

            log.info("Transactions evicted from hash.");
        }
    }

    @Override
    public void evictTransaction(String id) {
        if (id != null && !id.isEmpty()) {
            log.info("Evicting transaction for ID: {}", id);

            stringRedisTemplate.delete(id);

            log.info("Transaction evicted.");
        }
    }

    @Override
    public void evictTransactionFromHash(String id) {
        if (id != null && !id.isEmpty()) {
            log.info("Evicting transaction from hash for ID: {}", id);

            stringRedisTemplate.delete(id);

            log.info("Transaction evicted from hash.");
        }
    }
}