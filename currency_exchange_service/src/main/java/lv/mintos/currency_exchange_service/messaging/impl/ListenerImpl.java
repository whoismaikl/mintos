package lv.mintos.currency_exchange_service.messaging.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import lv.mintos.currency_exchange_service.caching.CacheService;
import lv.mintos.currency_exchange_service.messaging.Listener;
import lv.mintos.currency_exchange_service.service.CurrencyExchangeService;
import lv.mintos.shared_libraries.dto.ExchangeRateDto;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class ListenerImpl implements Listener {

    final CacheService cacheService;
    final CurrencyExchangeService currencyExchangeService;

    public ListenerImpl(CacheService cacheService, CurrencyExchangeService currencyExchangeService) {
        this.cacheService = cacheService;
        this.currencyExchangeService = currencyExchangeService;
    }

    @RabbitListener(queues = "currencyQueue")
    @Override
    public void handleExchangeRateRequest(ExchangeRateDto exchangeRateDto, Channel channel, long tag) {

        try {
            log.info("Received exchangeRateDto: " + exchangeRateDto.toString());
            currencyExchangeService.getExchangeRate(exchangeRateDto);
        } catch (RuntimeException e) {
            log.error("RuntimeException error occurred in currency exchange listener", e);
            this.safeBasicReject(channel, tag);
            throw new AmqpRejectAndDontRequeueException(e);
        } catch (JsonProcessingException e) {
            this.safeBasicReject(channel, tag);
            throw new AmqpRejectAndDontRequeueException(e);
        }

    }

    private void safeBasicReject(Channel channel, long tag) {
        try {
            log.info("Calling safeBasicReject in currency listener");
            channel.basicReject(tag, false);
        } catch (IOException e) {
            log.error("Currency Service: Failed to reject message", e);
        }
    }

}
