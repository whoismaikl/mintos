package lv.mintos.currency_exchange_service.messaging;

import com.rabbitmq.client.Channel;
import lv.mintos.shared_libraries.dto.ExchangeRateDto;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;

import java.io.IOException;

public interface Listener {

    void handleExchangeRateRequest(ExchangeRateDto exchangeRateDto, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws IOException;

}
